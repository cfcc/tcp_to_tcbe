FROM php:8-apache-buster

RUN \
apt-get update \
    && apt-get install -y --no-install-recommends \
    libldap2-dev \
    unixodbc-dev \
    gnupg \
    libssh2-1 \
    libssh2-1-dev \
    && curl -fsSL 'https://packages.microsoft.com/keys/microsoft.asc' | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get update \
    && ACCEPT_EULA=Y apt-get install -y --no-install-recommends msodbcsql17 \
    && rm -rf /var/lib/apt/lists/* \
    && pecl install sqlsrv pdo_sqlsrv ssh2-1.3.1\
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap \
    && docker-php-ext-enable sqlsrv pdo_sqlsrv ssh2

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
COPY start-apache.sh /usr/local/bin
# RUN a2enmod rewrite

# Copy application source
COPY src /var/www/html
# Copy required packages supplied through Composer
COPY vendor /var/www/html/vendor

RUN chown -R www-data:www-data /var/www

CMD ["start-apache.sh"]

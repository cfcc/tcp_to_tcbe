# Time Clock Plus to (Colleague) Time Card Batch Entry

A user can upload the CSV file produced by TCP (ex: "July 2021 Leave Taken.csv") and it will be converted and uploaded
to the Colleague destination folder so that the Payroll department can import it with TCBE.

The Colleague ODS database is used to look up the employee position codes.

## Docker Image

The docker image is based on the [Official PHP image](https://hub.docker.com/_/php/), specifically PHP 8 running on
Debian Buster with Apache.

## SSH Configuration

The PHP ssh2 library requires the private/pubic keys to be in PEM format.  You can generate them with:

    ssh-keygen -m PEM -t rsa -f id_rsa

The files are stored in ./instance/.ssh/ and should be owned by www-data so that the Apache process can read them.

### PHP SSH configuration in settings.ini

To capture the hosts fingerprint you can use the following commands in bash:

     ssh-keygen -E md5 -lf <(ssh-keyscan HOSTNAME_HERE 2>/dev/null) | grep '(RSA)' | cut -d ' ' -f2 | sed 's/MD5://' | sed 's/://g' | sed 's/[a-z]/\U&/g'

That will generate an uppercase version of the MD5 host fingerprint that PHP can use to validate the server when connecting.

## Composer and phpseclib3

The built-in SSH2 library doesn't handle timeouts well so I am experimenting with phpseclib v3.  It requires composer to install.

* Download and install the composer script in ./bin (instructions from https://getcomposer.org/download/)

     make install-composer

* Install the packages listed in composer.json

     php ./bin/composer.phar install

The files will then be ready in the vendor subfolder.

## Docker and WSL on Windows 10

I followed the directions from https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly

For Windows the volume mounts only seem to work from /c/Users/.../
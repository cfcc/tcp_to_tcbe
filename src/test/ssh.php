<?php
require __DIR__ . '/../vendor/autoload.php';

use phpseclib3\Net\SSH2;
use phpseclib3\Crypt\PublicKeyLoader;

include_once "../include/appConfig.php";

function my_ssh_disconnect($reason, $message, $language) {
    printf("Server disconnected with reason code [%d] and message: %s\n",
        $reason, $message);
}

$config = load_config();

$username = $config['ssh']['username'];
$pub_key_file = $config['ssh']['pub_key_file'];
$priv_key_file = $config['ssh']['priv_key_file'];

// This in order to be sure apache can read public key
// (remove this after debug)
$pub_key = file_get_contents($pub_key_file);
print "<pre>";
var_export($pub_key);
print "</pre>";

// This in order to check private one
// (remove this after debug)
$prv_key = file_get_contents($priv_key_file);
print "<pre>";
var_export($prv_key);
print "</pre>";

// Finally the connection code
$connection = ssh2_connect($config['ssh']['hostname'], 22, array('hostkey', 'ssh-rsa,ssh-dss'), array('disconnect', 'my_ssh_disconnect'));
if (ssh2_auth_pubkey_file($connection, $username, $pub_key_file, $priv_key_file)) {
    echo "Public Key Authentication Successful\n";
} else {
    echo "Public Key Authentication Failed";
}

echo "<hr>";

echo "<p>Testing phpseclib SFTP</p>";

$key = PublicKeyLoader::load($prv_key);

$ssh = new SSH2($config['ssh']['hostname']);
if (!$ssh->login($username, $key)) {
    throw new \Exception('Login failed');
} else {
    echo "Public Key Authentication Successful\n";
}

<?php

require_once "../include/functions.php";
require_once "../include/database.php";

$employee_ids = array('0075580', '0075952', '0080182', '0081595', '0082961', '0084890', '0209699');

try {
    $db = get_database();
    foreach ($employee_ids as $emp_id) {
        $position = get_position_code($emp_id, $db);
        print("<p>Position code for $emp_id:</p>\n<pre>");
        print_r($position);
        print("</pre>\n");
    }
} catch (Exception $e) {
    print("Failed to connect to the database: " . $e->getMessage());
}

/**
 * Created by jfriant80 on 8/22/16.
 */
function do_login(next_url, base_url) {
    var username = $('#username').val();
    var password = $('#password').val();
    if (username === "") {
        alert("Username cannot be empty");
        $('#username').focus();
        return false;
    }
    if (password === "") {
        alert("Password cannot be empty");
        $('#password').focus();
        return false;
    }
    if (!base_url) { base_url = ""}
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: base_url + 'include/login_actions_ajax.php',
        data:{
            username:username,
            password:password
        },
        success:function(data,textStatus){
            if (is_valid_domain(next_url)) {
                window.location=next_url;
            } else {
                window.location.reload(true);
            }
        },
        error: function (xhr,textStatus, errorThrown){
            alert("Login Failed.  Username or password incorrect.");
            console.log('Login failed with error [' + errorThrown + ']');
        }
    });
    return false;
}

function do_logout(include_path, next_url) {
    $.ajax({
        type:'POST',
        dataType: 'text',
        url: include_path + 'include/logout_actions_ajax.php',
        data:{},
        success:function(data,textStatus){
            if (is_valid_domain(next_url)) {
                window.location=next_url;
            } else {
                window.location.reload(true);
            }
        },
        error: function (xhr,textStatus, errorThrown){
            alert(errorThrown);
        }
    });
    return false;
}

function is_valid_domain(url) {
    var patterns = {};
    var success = false;

    patterns.protocol = '^^(http(s)?(:\/\/))?(www\.)?';
    patterns.domain = '([a-zA-Z0-9-_\.]+)';

    var url_regex = new RegExp(patterns.protocol + patterns.domain, 'gi');
    var match = url_regex.exec(url);
    if (match) {
        console.log('[DEBUG] ' + match[5] + " == " + window.location.hostname);
        if (match[5] === window.location.hostname){
            // FQDN is good if it matches the server
            success = true;
        } else if (match[5] === '..' || match[5] === '.') {
            // Relative path OK too
            success = true;
        }
    }
    return success;
}

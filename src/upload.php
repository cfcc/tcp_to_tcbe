<?php
# process the CSV file and transfer it to Colleague via SSH
require_once "include/appConfig.php";
require_once "include/database.php";
require_once "include/functions.php";
require_once "include/Uploader.php";

sec_session_start();

$config = load_config();
try {
    $ods_db = get_database();
} catch (Exception $e) {
    $ods_db = NULL;
    $fatal_error = print_r($e->getMessage(), true);
}

include_once "include/header.php";

$colors = array('Tomato', 'Orange', 'DodgerBlue', 'MediumSeaGreen', 'Gray', 'SlateBlue', 'Violet', 'LightGray');

if (login_check()) {
    if (!empty($_FILES['csvfile']) && $ods_db !== NULL) {
        if ($_FILES['csvfile']['size'] > 0) {
            if ($_POST['destenv'] != "" && array_key_exists($_POST['destenv'], $config['ssh']['environment'])) {
                try {
                    error_log('processing CSV file...');
                    print("<p>Processing CSV file...</p>\n");
                    print('<div class="scroll">');
                    $csv_fn = $_FILES['csvfile']['tmp_name'];
                    $my_uploader = new Uploader($config, $_POST['destenv']);
                    error_log('converting records...');
                    $my_uploader->load_csv($csv_fn, $ods_db);
                    foreach ($my_uploader->get_records() as $leave_records) {
                        foreach ($leave_records as $line) {
                            print("<p>");
                            for ($i = 0, $len = count($line); $i < $len; $i++) {
                                print("<span style='color: {$colors[$i]}'>");
                                print($line[$i]);
                                print("</span>\n");
                            }
                            print("</p>\n");
                        }
                        print("<hr>");
                    }
                    print("</div>");
                    print("<pre>\n");
                    error_log('uploading to Colleague...');
                    print("Uploading file to Colleague... ");
                    $my_uploader->do_upload();
                    print("Done");
                    print("</pre>\n");
                    print("<a href='index.php' class='btn btn-outline-success'>Close</a>\n");
                } catch (Exception $e) {
                    print("<div class='text-danger'>\n<p>Error Uploading the file:</p><pre>" . $e->getMessage() . "</pre></div>");
                    print("<a href='index.php' class='btn btn-outline-secondary'>Close</a>\n");
                }
            } else {
                print("<p class='lead text-danger'>Error: invalid environment path given.</p>");
            }
        } else {
            if ($_FILES['csvfile']['error'] == UPLOAD_ERR_INI_SIZE) {
                print("<p class='lead text-danger'>Error: the file was too large to import.</p>");
            } else {
                print("<p class='lead text-warning'>Unable to import an empty file.</p>");
            }
            // print("<pre>" . print_r($_FILES['csvfile'], true) . "</pre>");
            print("<a href='index.php' class='btn btn-outline-secondary'>Close</a>");
        }
    }
} else {
    echo "You must be logged in.";
}

require_once "include/footer.php";

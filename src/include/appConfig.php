<?php

const DEFAULT_LOCATION = '/home/tcp2tcbe/settings.ini';

function load_config(string $user_config_file=NULL): array {
    if ($user_config_file == NULL) {
        $config_location = DEFAULT_LOCATION;
    } else {
        $config_location = $user_config_file;
    }
    return parse_ini_file($config_location, true);
}

<?php
require_once "cfcc-ad-authentication/AuthenticationModuleClass.php";
require_once "appConfig.php";
require_once "functions.php";

sec_session_start();

try {
    $config = load_config();
    $authenticator = new AuthenticationModule();
    $authentication_result = $authenticator->authenticate($_POST['username'], $_POST['password']);
    if ($authentication_result) {
            $_SESSION['user_info'] = $authenticator->get_user_info();
            $_SESSION['access'] = authorize($_SESSION['user_info']['groups'], $config);

            header('HTTP/1.1 200 Authentication: Success');
    } else {
            header('HTTP/1.1 401 Authentication: Failure');
    }
} catch (Exception $e) {
    header('HTTP/1.1 500 ' . "Caught Exception:" . $e->getMessage());
}
exit;

<?php
$rel_path = $include_path ?? "";

if (!isset($logout_url)) { $logout_url = "/"; }

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    <link href="css/site.css" rel="stylesheet">

    <title>TCP to TCBE</title>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">T2T</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav me-auto mb-2 mb-md-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                </li>
                <!--
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled">Disabled</a>
                </li>
                -->
            </ul>
            <form class="d-flex" method="post">
                <?php
                $result = login_check();
                if ($result) {
                    echo "<span class=\"navbar-text me-2\">Signed in as " . $_SESSION['user_info']['username'] . "</span>";
                    echo "<input type=\"submit\" class=\"btn btn-outline-success\" onclick=\"return do_logout('" . $rel_path . "', '" . $logout_url . "');\" value=\"Logout\">\n";
                } else {
                    echo '<input class="form-control me-2" type="text" id="username" placeholder="Username" aria-label="Username" autofocus>';
                    echo '<input class="form-control me-2" type="password" id="password" placeholder="Password" aria-label="Password">';
                    echo '<button type="submit" class="btn btn-outline-success" onclick="return do_login(\'\', \'' . $rel_path . '\');">Login</button>';
                }
                ?>
            </form>
        </div>
    </div>
</nav>

<main class="container">

<?php

if (!empty($page_title)) {
    echo "<div class='page-header'><h1>" . $page_title . "</h1></div>\n";
}
if (isset($_SESSION['flash'])) {
    echo "<div class='row'>\n";
    echo "<div class='alert alert-warning' role='alert'>\n";
    echo "<span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span>\n";
    echo "<span class=\"sr-only\">Warning:</span>\n";
    echo $_SESSION['flash'] . "\n";
    echo "</div>\n";
    echo "</div>\n";
    unset($_SESSION['flash']);
}
if (isset($fatal_error)) {
    echo "<div class='row'>\n";
    echo "<div class='alert alert-danger' role='alert'>\n";
    echo "<span class=\"glyphicon glyphicon-warning-sign\" aria-hidden=\"true\"></span>\n";
    echo "<span class=\"sr-only\">Error:</span>\n";
    echo "<b>Fatal Error:</b> " . $fatal_error . "\n";
    echo "</div>\n";
    echo "</div>\n";
    unset($fatal_error);
}

<?php
require __DIR__ . '/../vendor/autoload.php';

use phpseclib3\Net\SFTP;
use phpseclib3\Crypt\PublicKeyLoader;

define('NET_SFTP_LOGGING', SFTP::LOG_SIMPLE);

/* Notify the user if the server terminates the connection */
function my_ssh_disconnect($reason, $message, $language) {
    printf("Server disconnected with reason code [%d] and message: %s\n",
        $reason, $message);
}

class niceSSH
{
    private $ssh_host = "";
    private $ssh_port = 22;
    // SSH Server Fingerprint
    private $ssh_server_fp = "";
    // SSH Username
    private $ssh_auth_user = "";
    // SSH Public Key File
    private $ssh_auth_pub = "";
    // SSH Private Key File
    private $ssh_auth_priv = "";
    // SSH Private Key Passphrase (null == no passphrase)
    private $ssh_auth_pass;
    private $connection;

    private $methods = array(
        'hostkey'=>'ssh-rsa,ssh-dss',
        //    'kex' => 'diffie-hellman-group-exchange-sha256',
        'client_to_server' => array(
            'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
            'comp' => 'none'),
        'server_to_client' => array(
            'crypt' => 'aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc,blowfish-cbc',
            'comp' => 'none'));

    private $callbacks = array('disconnect' => 'my_ssh_disconnect');

    public function __construct(string $hostname, string $fp, string $username, string $pub_key_file, string $priv_key_file) {
        $this->ssh_host = $hostname;
        $this->ssh_server_fp = $fp;
        $this->ssh_auth_user = $username;
        $this->ssh_auth_pub = $pub_key_file;
        $this->ssh_auth_priv = $priv_key_file;
    }

    // public function connect_orig() {
    //     if (!($this->connection = ssh2_connect($this->ssh_host, $this->ssh_port, $this->methods, $this->callbacks))) {
    //         throw new Exception('Failed to connect to server');
    //     }
    //     $fingerprint = ssh2_fingerprint($this->connection, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);
    //     if (strcmp($this->ssh_server_fp, $fingerprint) !== 0) {
    //         throw new Exception('Unable to verify server identity: '. $fingerprint );
    //     }
    //     if (!ssh2_auth_pubkey_file($this->connection, $this->ssh_auth_user, $this->ssh_auth_pub, $this->ssh_auth_priv)) {
    //         throw new Exception('Authentication rejected by server');
    //     };
    // }

    public function connect() {
        $key = PublicKeyLoader::load(file_get_contents($this->ssh_auth_priv));
        $this->connection = new SFTP($this->ssh_host);
        if (!$this->connection->login($this->ssh_auth_user, $key)) {
            throw new \Exception('SFTP Authentication failed');
        }
    }

    // public function exec($cmd) {
    //     if (!($stream = ssh2_exec($this->connection, $cmd))) {
    //         throw new Exception('SSH command failed');
    //     }
    //     stream_set_blocking($stream, true);
    //     $data = "";
    //     while ($buf = fread($stream, 4096)) {
    //         $data .= $buf;
    //     }
    //     fclose($stream);
    //     return $data;
    // }

    // public function upload_orig($contents, $remote_filename) {
    //     try {
    //         $sftp = ssh2_sftp($this->connection);
    //         if (!$sftp) {
    //             throw new Exception("Could not initialize SFTP subsystem");
    //         }
    //         $sftp_stream = @fopen('ssh2.sftp://' . intval($sftp) . $remote_filename, 'w');
    //         if (!$sftp_stream) {
    //             throw new Exception("Could not open remote file: $remote_filename");
    //         }
    //         if (@fwrite($sftp_stream, $contents) === false) {
    //             throw new Exception("Could not write data to remote file");
    //         }
    //         fclose($sftp_stream);
    //     } catch (Exception $e) {
    //         error_log('Exception: ' . $e->getMessage());
    //         if ($sftp_stream) {
    //             fclose($sftp_stream);
    //         }
    //     }
    // }

    public function upload($contents, $remote_filename): bool {
        $ignore_errors = array("SSH_MSG_GLOBAL_REQUEST");
        $success = true;
        $this->connection->put($remote_filename, $contents);
        foreach ($this->connection->getSFTPLog() as $line) {
            error_log($line);
        }
        $errors = $this->connection->getErrors();
        if (count($errors) > 0) {
            # Some of these errors can be ignored, so we'll just log them
            foreach ($errors as $msg) {
                $can_ignore = false;
                foreach ($ignore_errors as $ignore_this) {
                    if (str_starts_with($msg, $ignore_this)) {
                        $can_ignore = true;
                        break;
                    }
                }
                error_log($msg);
                if (!$can_ignore) {
                    $success = false;
                }
            }
        }
        return $success;
    }

    // public function scp($contents, $remote_filename) {
    //     try {
    //         $tmpfname = tempnam("/tmp", "TCP");
    //         $handle = fopen($tmpfname, "w");
    //         fwrite($handle, $contents);
    //         fclose($handle);
    //         $result = ssh2_scp_send($this->connection, $tmpfname, $remote_filename);
    //         unlink($tmpfname);
    //     } catch (Exception $e) {
    //         error_log('Exception: ' . $e->getMessage());
    //         print ("[ERROR] Failed to transfer file.");
    //     }
    // }

    public function disconnect() {
        if ($this->connection) {
            $this->connection->exec('echo "EXITING" && exit;');
            $this->connection = null;
        }
    }

    public function __destruct() {
        $this->disconnect();
    }
}

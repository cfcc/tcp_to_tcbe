<?php
include_once 'functions.php';

try {

    sec_session_start();

    // Unset all session values
    $_SESSION = array();

    // get session parameters
    $params = session_get_cookie_params();

    // delete the actual cookie
    setcookie(session_name(),
        '', time() - 42000,
        $params['path'],
        $params['domain'],
        $params['secure'],
        $params['httponly']);

    // destroy the session
    session_destroy();

    header('HTTP/1.1 200 Authentication: Logged Out');
} catch (Exception $e) {
    header('HTTP/1.1 500 ' . "Caught Exception:" . $e->getMessage());
}

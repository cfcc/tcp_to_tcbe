<?php

require_once 'appConfig.php';

/**
 * @throws Exception
 */
function get_database(): PDO
{
    $config = load_config();

    $options = array(
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );
    try {
        $dsn = "sqlsrv:server=".$config['ods']['host']."; Database=".$config['ods']['dbname'];
        $ods_db = new PDO($dsn, $config['ods']['username'], $config['ods']['password']);
        $ods_db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
        # header("Location: /error.php?err=" + print_r($e->getMessage(), true), true, 302);
        throw new Exception(print_r($e->getMessage(), true) . " for MS SQL Database");
    }
    return $ods_db;
}

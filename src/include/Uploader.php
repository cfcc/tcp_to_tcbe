<?php

use JetBrains\PhpStorm\Pure;

const COL_EMP_ID = 0;
const COL_POSITION_CODE = 1;
const COL_POST_DATE = 2;
const COL_JOB_CODE = 3;
const COL_HOURS = 4;

function parse_csv_header(array $header): array
{
    $col_index = array();
    $header_len = count($header);
    for ($i=0; $i < $header_len; $i++) {
        $col_name = substr($header[$i], 1, -1);
        $col_index[$col_name] = $i;
    }
    return $col_index;
}

class Uploader
{
    private array $records = array();
    private array $merged_records = array();
    private string $pay_cycle_code = 'MO';
    private DateTime $pay_cycle_end_date;
    private int $year;
    private int $month;
    private niceSSH $ssh;
    private string $remote_directory;
    private array $job_codes;

    #[Pure] public function __construct($config, $dest_env) {
        $this->ssh = new niceSSH(
            $config['ssh']['hostname'],
            $config['ssh']['fingerprint'],
            $config['ssh']['username'],
            $config['ssh']['pub_key_file'],
            $config['ssh']['priv_key_file']
        );
        if (array_key_exists($dest_env, $config['ssh']['environment'])) {
            $this->remote_directory = $config['ssh']['environment'][$dest_env];
        } else {
            $this->remote_directory = "";
        }
        $this->job_codes = $config['general']['job_codes'];
    }

    /** load_csv
     * @param string $filename - has the name of the CSV file
     * @param mixed $db - has the database cursor
     * @throws Exception - when the CSV header is missing
     */
    public function load_csv(string $filename, mixed $db) {
        $handle = fopen($filename, 'r');
        $line_cnt = 0;
        if ($handle !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $line_cnt++;
                if (str_starts_with($data[0], "<")) {
                    $col_index = parse_csv_header($data);
                } else {
                    if (!isset($col_index)) {
                        throw new Exception('CSV file was missing the header, unable to continue');
                    }
                    if ($data[$col_index['JOBCODE']] !== "") {
                        if (array_key_exists($data[$col_index['JOBCODE']], $this->job_codes)) {
                            $job_code = $this->job_codes[$data[$col_index['JOBCODE']]];
                        } else {
                            throw new Exception("Invalid job code on line $line_cnt:\n". print_r($data, true));
                        }
                        $emp_id = $data[$col_index['ECODE']];
                        $position = get_position_code($emp_id, $db);
                        $post_date = DateTime::createFromFormat("m/d/Y", $data[$col_index['POSTDATE']]);
                        if ($post_date) {
                            $post_date_key = $post_date->format("Ymd");
                            $this->records[$emp_id][$post_date_key] = array($emp_id, $position, $post_date, $job_code, $data[$col_index['HRSTAKEN']]);
                        } else {
                            throw new Exception("Error parsing date on line $line_cnt:\n" . print_r($data, true));
                        }
                    }
                }
            }
        }
        fclose($handle);

        $last_emp = end($this->records);
        $last_date = end($last_emp)[COL_POST_DATE];
        $this->year = $last_date->format('Y');
        $this->month = $last_date->format('m');

        if ($this->month < 12) {
            $number = days_in_month($this->month + 1, $this->year);
        } else {
            $number = 31; // January
        }
        $this->pay_cycle_end_date = new DateTime();
        $this->pay_cycle_end_date->setDate($this->year, $this->month + 1, $number);
    }

    /**
     * Merge the leave records with the work calendar days for this month and return an array.  The merged records
     * are stored, so they don't have to be rebuilt each time.
     *
     * According to "41917: Colleague Payroll - Using Time Card Batch Entry (TCBE/TCBS) for Salaried Employees Leave"
     * we need to fill out the hours for exempt people.  Not sure how this works for non-exempt yet.
     */
    public function get_records($fill_hours=false): array
    {
        if (empty($this->merged_records)) {
            foreach ($this->records as $emp_id => $leave_recs) {
                $exempt = end($leave_recs)[COL_POSITION_CODE]['status'] = (bool)'S';
                if ($exempt && $fill_hours) {
                    try {
                        $work_days = build_month(
                            $this->month,
                            $this->year,
                            $this->pay_cycle_code,
                            $this->pay_cycle_end_date->format("m/d/Y"),
                            $emp_id,
                            end($leave_recs)[COL_POSITION_CODE]['code']
                        );
                    } catch (TypeError $e) {
                        print "<pre>\n";
                        print "Error: " . $e->getMessage();
                        print_r($leave_recs[0]);
                        print ("\n</pre>\n");
                    }
                } else {
                    // Non-exempt employees (hourly) do not need the days filled out, just the leave taken
                    $work_days = array();
                }
                foreach ($leave_recs as $key => $line) {
                    // Any leave taken that is less than a day needs regular hours too
                    $leave_hours = floatval($line[COL_HOURS]);
                    if ($leave_hours < 8.0 && $exempt && $fill_hours) {
                        $regular_hours = 8.0 - $leave_hours;
                        $work_days[$key . "reg"] = array(
                            $this->pay_cycle_code,
                            $this->pay_cycle_end_date->format("m/d/Y"),
                            $emp_id,
                            $line[COL_POSITION_CODE]['code'],
                            $line[COL_POST_DATE]->format("m/d/Y"),
                            "NI", // FIXME: this might need to be based on the position too
                            number_format($regular_hours, 2, ".", ","),
                        );
                    }
                    $work_days[$key] = array(
                        $this->pay_cycle_code,
                        $this->pay_cycle_end_date->format("m/d/Y"),
                        $emp_id,
                        $line[COL_POSITION_CODE]['code'],
                        $line[COL_POST_DATE]->format("m/d/Y"),
                        $line[COL_JOB_CODE],
                        $line[COL_HOURS],
                    );
                }
                ksort($work_days);
                $this->merged_records[$emp_id] = $work_days;
            }
        }
        return $this->merged_records;
    }

    public function csv_to_fixed(): string
    {
        # Line format (from TCRD):
        #  Pay Cycle: 1 - 2                 length =  2
        #  Period Ending Date: 3 - 12       length = 10
        #  Employee ID: 13 - 19             length =  7
        #  Position ID: 20 - 33             length = 14
        #  Work Date: 34 - 43               length = 10
        #  Earnings Type: 44 - 47           length =  4
        #  Hours Worked: 48 - 55            length =  8
        #
        # EX:
        # .........1.........2.........3.........4.........5.....
        # 1234567890123456789012345678901234567890123456789012345
        # MO06/01/20190640590FSIANTHRPLGY0909/21/2021TP      8.00
        $lines_out = "";
        foreach ($this->get_records() as $leave_records) {
            foreach ($leave_records as $line) {
                $lines_out .= $line[0];
                $lines_out .= $line[1];
                $lines_out .= $line[2];
                $lines_out .= str_pad($line[3], 14);
                $lines_out .= $line[4];
                $lines_out .= str_pad($line[5], 4);
                $lines_out .= str_pad($line[6], 8, " ", STR_PAD_LEFT);
                $lines_out .= "\n";
            }
        }
        return $lines_out;
    }

    /**
     * @throws Exception
     */
    public function do_upload() {
        $remote_fn = $this->remote_directory . "/TCBE_" . $this->pay_cycle_end_date->format("Y-m-d") . ".TXT";
        error_log('[SSH] Connecting to host...');
        $this->ssh->connect();
        error_log('[SSH] uploading...');
        $result = $this->ssh->upload($this->csv_to_fixed(), $remote_fn);
        // $this->ssh->scp($this->csv_to_fixed(), $remote_fn);
        error_log('[SSH] closing connection...');
        $this->ssh->disconnect();
        if ($result) {
            print("$remote_fn ...");
        } else {
            print("FAILED TO TRANSFER FILE");
        }
    }
}

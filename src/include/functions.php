<?php

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

require_once "niceSSH.php";

// Access levels
const ACCESS_DENIED = 0;
const ACCESS_AUTHENTICATED = 1;
const ACCESS_USER = 2;
const ACCESS_MANAGER = 3;
const ACCESS_ADMINISTRATOR = 4;

// The login functions below come from the tutorial on wikihow:
// http://www.wikihow.com/Create-a-Secure-Login-Script-in-PHP-and-MySQL

function sec_session_start() {
    $session_name = 'sec_session_id';   // Set a custom session name
    // This makes HTTPS required for sessions
    $secure = true;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === FALSE) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"],
        $cookieParams["path"],
        $cookieParams["domain"],
        $secure,
        $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id(true);    // regenerated the session, delete the old one.
}

/**
 * Compare the user's groups with the access levels in the configuration and return the level of access that the user
 * is authorized for.  By default they will be logged in but with no extra access.
 *
 * @param array $groups - The LDAP groups returned by CFCC-AD-Authentication
 * @param array $config - the config file contents
 * @return int - the access level, 0 - 2
 */
function authorize(array $groups, array $config): int {
    $access = ACCESS_AUTHENTICATED;

    // Active Directory user groups
    $ldap_user_groups = $config['ad']['user_groups'];

    // Active Directory manager groups
    $ldap_manager_groups = $config['ad']['manager_groups'];

    // Active Directory administrator groups
    $ldap_administrator_groups = $config['ad']['administrator_groups'];

    // check groups
    foreach ($groups as $group_name) {
        foreach ($ldap_administrator_groups as $admin_group_name) {
            if ($group_name == $admin_group_name) {
                $access = ACCESS_ADMINISTRATOR;
                break;
            }
        }
        # administrators have full access, no need for further checks
        if ($access == ACCESS_ADMINISTRATOR) {
            break;
        }

        foreach ($ldap_manager_groups as $mgr_group_name) {
            if ($group_name == $mgr_group_name) {
                $access = ACCESS_MANAGER;
                break;
            }
        }
        // is manager, break loop
        if ($access == ACCESS_MANAGER) {
            break;
        }

        // is user, but keep checking for a manager group
        foreach ($ldap_user_groups as $user_group_name) {
            if ($group_name == $user_group_name) $access = ACCESS_USER;
        }
    }
    return $access;
}

/**
 * @param int $required_access
 * @return bool
 */
function login_check(int $required_access = ACCESS_AUTHENTICATED): bool
{
    $access = ACCESS_DENIED;
    if (isset($_SESSION['user_info']) && isset($_SESSION['access'])) {
        $access = $_SESSION['access'];
    }
    #error_log("login_check_ldap(): " . $access . " >= " . $required_access);
    return ($access >= $required_access);
}

function days_in_month(int $month, int $year): int
{
    // calculate number of days in a month
    return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
}

#[Pure] function find_pay_cycle_end(string $date_in): string
{
    $last_date = date_parse($date_in);
    $number = days_in_month($last_date['month'], $last_date['year']);
    return str_pad($last_date['month'], 2, "0", STR_PAD_LEFT) . "/" . str_pad($number, 2, "0", STR_PAD_LEFT) . "/" . $last_date['year'];
}

function build_month(int $month, int $year, string $pc_code='', string $pc_end_date='', string $emp_id='', string $pos_code='', $pay_code='NI'): array
{
    $month_work_days = array();

    for ($i=1, $number_of_days = days_in_month($month, $year); $i<=$number_of_days; $i++) {
        $this_day = new DateTime("$year-$month-$i");
        $this_day_of_week = date("w", $this_day->getTimestamp());
        if ($this_day_of_week > 0 && $this_day_of_week < 6) {
            $key = $this_day->format('Ymd');
            $month_work_days[$key] = array(
                $pc_code,
                $pc_end_date,
                $emp_id,
                $pos_code,
                $this_day->format("m/d/Y"),
                $pay_code,
                "8.00"
            );
        }
    }
    return $month_work_days;
}

/**
 * @param $emp_id - the Colleague person ID
 * @param $db - the database cursor
 * @return array
 */
#[ArrayShape(['code' => "mixed", 'status' => "mixed"])] function get_position_code(string $emp_id, PDO $db): array
{
    $today = new DateTime();
    $query = 'SELECT PERPOS_POSITION_ID, PERPOS_HRLY_OR_SLRY FROM SPT_PERSTAT
    LEFT JOIN SPT_PERPOS ON PERSTAT_PRIMARY_PERPOS_ID = PERPOS_ID
    WHERE PERSTAT_HRP_ID=?
    -- AND (PERSTAT_END_DATE IS NULL OR PERSTAT_END_DATE>=?)
    ORDER BY PERSTAT_START_DATE DESC';
    $stmt = $db->prepare($query);
    $stmt->execute(array($emp_id, $today->format('Y-m-d')));
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if (is_array($result) && count($result) > 0) {
        $position = array(
            'code' => $result[0]['PERPOS_POSITION_ID'],
            'status' => $result[0]['PERPOS_HRLY_OR_SLRY']
        );
    } else {
        $position = array('code' => '', 'status' => '');
    }
    return $position;
}

<?php
require_once "include/functions.php";

sec_session_start();

include_once "include/header.php";

if (login_check()) {
    ?>

    <div class="bg-light p-5 rounded">
        <h1>Upload to Colleague</h1>
        <form enctype="multipart/form-data" method="post" id="tcpUpload" name="tcpUpload" action="upload.php">
            <div class="mb-3">
                <label for="csvfile" class="form-label">Use this form to upload the CSV file exported from Time Clock Plus</label>
                <input class="form-control" type="file" id="csvfile" name="csvfile" required>
            </div>
            <div class="mb-3">
                <select class="form-select" id="destenv" name="destenv" aria-label="Select upload destination" required>
                    <option value="">Select upload destination</option>
                    <option value="prod">Colleague Production</option>
                    <option value="test1">Colleague Test 1</option>
                    <option value="test2">Colleague Test 2</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <?php
} else {
    ?>
    <div class="pt-5 mx-auto" style="width: 800px;">
        <img src='images/logo_onecolor_lores_watermark.jpg' alt='CFCC Logo'>
    </div>
    <?php
}
require_once "include/footer.php";

app_name = tcp2tcbe
win_home = /c/Users/jfriant80/src/tcp_to_tcbe
export SERVICE_NAME=$(app_name)
build:
	@build-image.sh
run:
	docker run --detach -p 8080:80 -v $(win_home)/instance:/home/tcp2tcbe/:ro $(app_name)
kill:
	@echo 'Killing container...'
	@docker ps | grep $(app_name) | awk '{print $$1}' | xargs docker stop
rebuild: kill build run
tag:
	@docker tag $(app_name) nexus.ad.cfcc.edu:18095/$(app_name)
push: build tag
	@docker push nexus.ad.cfcc.edu:18095/$(app_name)
install-composer:
	mkdir -p ./bin
	@php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	@php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	@php composer-setup.php --install-dir=./bin
	@php -r "unlink('composer-setup.php');"
